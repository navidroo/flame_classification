# Flame classification

Flame classification is a Python library for classifying the flame types observed within an externally heated micro-channel.

## Introduction

This package generates data set files, augment data, and use neural network for classifying the flame types using a supervised learning method.
Flame types in this package are:
1) Weak flame
2) Flames with repetitive extiction and ignition (FREI)
3) Stable flame

## Installation

### Conda

The following conda environment was tested:

```
$ conda env create -f environment.yml
```
Pull a local copy of the repository outside of the python path, and check out the latest stable branch. The following uses pip to install a (linked) version within the current python environment.

```
$ git clone git@gitlab.com:navidroo/flame_classification.git
$ cd flame classification

````

### Update
Within the source folder, run:
```
$ git pull
$ python setup.py develop
````

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
